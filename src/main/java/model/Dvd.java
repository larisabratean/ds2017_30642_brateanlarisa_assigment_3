package model;

import java.io.Serializable;

public class Dvd implements Serializable {

    private String title;
    private int year;
    private double price;

    public Dvd() {
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String toString() {
        return "title " + this.title + ", year: " + this.year + " with price: " + this.price;
    }
}

