package presentation;

import model.Dvd;
import producer.Producer;
import receiver.Receiver;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ServletDvd")
public class ServletDvd extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        int year = Integer.parseInt(request.getParameter("year"));
        double price = Double.valueOf(request.getParameter("price"));

        Receiver consumer = new Receiver("queue");
        Thread consumerThread = new Thread(consumer);
        consumerThread.start();

        Producer producer = new Producer("queue");
        if (request.getParameter("addDvd") != null) {
            Dvd dvd = new Dvd();
            dvd.setTitle(title);
            dvd.setYear(year);
            dvd.setPrice(price);
            producer.sendMessage("A new DVD was added in the stock. Details: " + dvd.toString());
            response.sendRedirect("successfulOp.jsp");
           }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
