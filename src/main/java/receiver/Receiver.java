package receiver;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;
import common.EndPoint;
import org.apache.commons.lang.SerializationUtils;

import java.io.IOException;

public class Receiver extends EndPoint implements Runnable, Consumer {

    private static int fileCount = 0;

    public Receiver(String endPointName) throws IOException {
        super(endPointName);
    }

    @Override
    public void run() {
        try {
            channel.basicConsume(endPointName, true, this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleConsumeOk(String s) {

    }

    @Override
    public void handleCancelOk(String s) {

    }

    @Override
    public void handleCancel(String s) throws IOException {

    }

    @Override
    public void handleShutdownSignal(String s, ShutdownSignalException e) {

    }

    @Override
    public void handleRecoverOk(String s) {

    }

    @Override
    public void handleDelivery(String s, Envelope envelope, AMQP.BasicProperties basicProperties, byte[] bytes) throws IOException {

        String message = (String) SerializationUtils.deserialize(bytes);
        MailService mailService = new MailService("larisabratean@gmail.com", "merginAmerica1*");
        FileService fileService = new FileService();
        System.out.println("Sending mail: " + message);
        mailService.sendMail("larisabratean@gmail.com", "New dvd added!", message);
        fileCount++;
        fileService.createNewFile(message, fileCount);
    }
}
