package receiver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileService{

    public void createNewFile(String message, int count){
        try {
            File fileWrite = new File("C:/Users/larisa/Desktop/Assignment_3_2/dvd" + count + ".txt");
            if (!fileWrite.exists()) {
                fileWrite.createNewFile();
            }
            FileWriter fileWriter = new FileWriter(fileWrite.getAbsoluteFile());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(message);
            bufferedWriter.close();
            System.out.println("The writing was successful!");
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}