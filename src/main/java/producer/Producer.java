package producer;

import common.EndPoint;
import org.apache.commons.lang.SerializationUtils;

import java.io.IOException;
import java.io.Serializable;

public class Producer extends EndPoint {
 public Producer(String endPointName) throws IOException{
     super(endPointName);
 }

 public void sendMessage(Serializable dvd) throws IOException{
     channel.basicPublish("", endPointName, null, SerializationUtils.serialize(dvd));
 }
}
