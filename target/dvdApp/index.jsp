<html>
<head>
    <title>Add DVD page</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
<form action="/ServletDvd" method="post">
    <div class="dvdWindow">
        DVD title: <input type="text" name="title" width="30"/>
        Year: <input type="number" name="year" width="30"/>
        Price: <input type="text" name="price" width="30"/>
        <input type="submit" name="addDvd" value="Add DVD"/>
    </div>
</form>
</body>
</head>
</html>